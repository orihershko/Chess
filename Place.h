#pragma once
class Place
{
public:
	Place(int x,int y);
	~Place();
	int getX() const;
	int getY() const;
	void operator+=(Place &other);
	bool operator==(const Place &other) const;
	void operator=(const Place &other);
private:
	int _X;
	int _Y;
};

