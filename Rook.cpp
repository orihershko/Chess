﻿#include "Rook.h"
#include <iostream>


Rook::Rook(const Place& startLocation,const char& type) : chessMen(startLocation,type)
{
}

Rook::~Rook()
{
}
int Rook::checkMove(const Place& newLocation , chessMen* board[8][8])
{
	int i = 0;
	
	if (_location.getX() != newLocation.getX() && _location.getY() != newLocation.getY())//error 6
	{
		throw int(ERROR_MOVE);
	}
	if (_location.getX() != newLocation.getX())
	{
		if (_location.getX() < newLocation.getX()) // for regular
		{
			i = _location.getX() + 1;
			for (i; i < newLocation.getX(); i++)
			{
				if (board[newLocation.getY()][i] != NULL)
				{
					throw int(ERROR_MOVE); 
				}
			}
		}
		else// for backing
		{
			i = newLocation.getX()+1;
			for (i; i < _location.getX(); i++)
			{
				if (board[newLocation.getY()][i] != NULL)
				{
					throw int(ERROR_MOVE);
				}
			}
		}
		_location = newLocation;
	}
	else if (_location.getY() != newLocation.getY())
	{
		if (_location.getY() < newLocation.getY())// for regular
		{
			i = _location.getY() + 1;
			for (i; i < newLocation.getY(); i++)
			{
				if (board[i][newLocation.getX()] != NULL)
				{
					throw int(ERROR_MOVE);
				}
			}
		}
		else// for backing
		{
			i = newLocation.getY() + 1;
			for (i; i < _location.getY(); i++)
			{
				if (board[i][newLocation.getX()] != NULL)
				{
					throw int(ERROR_MOVE);
				}
			}
		}

		_location = newLocation;
	}
	return GOOD_MOVE;

}
