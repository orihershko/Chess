#pragma once
#include "Place.h"
#define ERROR_MOVE 6
#define GOOD_MOVE 0
class chessMen
{
public:
	chessMen(const Place& startLocation, const char& type);
	~chessMen();
	virtual int checkMove(const Place& newPlace, chessMen* board[8][8]) = 0;
	char getType() const;
	Place getLocation() const;
protected:
	Place _location;
	char _type;
};
