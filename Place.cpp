#include "Place.h"



Place::Place(int X, int Y)
{
	_X = X;
	_Y = Y;
}


Place::~Place()
{
}
int Place::getX() const
{
	return _X;
}
int Place::getY() const
{
	return _Y;
}
void Place::operator+=(Place &other)
{
	_X += other.getX();
	_Y += other.getY();
}

bool Place::operator==(const Place &other) const
{
	return _X == other._X && _Y == other._Y;
}
void Place::operator=(const Place &other)
{
	_X = other._X;
	_Y = other._Y;
}