#pragma once
#include "chessMen.h"
#include "Rook.h"
#include "Bishop.h"
class Rook;
class Bisop;
class Queen :public chessMen
{
private:
	Rook _rook;
	Bishop _bishop;
public:
	Queen(const Place& location, const char type);
	int checkMove(const Place& newPlace , chessMen* board[8][8]);
	~Queen();
};

