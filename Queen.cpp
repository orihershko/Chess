#include "Queen.h"



Queen::Queen(const Place& location, const char type) : chessMen(location, type), _rook(location, type), _bishop(location, type)
{
}

int Queen::checkMove(const Place & newPlace , chessMen* board[8][8])
{

	if (_rook.checkMove(newPlace, board) == GOOD_MOVE || _bishop.checkMove(newPlace, board) == GOOD_MOVE)
	{
		_location = newPlace;
		return GOOD_MOVE;
	}
	

}


Queen::~Queen()
{
}
