#include "Knight.h"
#include<iostream>


Knight::Knight(const Place& location, const char type): chessMen(location, type)
{
	
}

int Knight::checkMove(const Place & newPlace , chessMen* board[8][8])
{
	if (board[newPlace.getY()][newPlace.getX()] != NULL)
	{
		if (isupper(_type) && isupper(board[newPlace.getY()][newPlace.getX()]->getType()) || islower(_type) && islower(board[newPlace.getY()][newPlace.getX()]->getType()))
		{
			throw int(ERROR_MOVE);
		}
	}
	if (newPlace.getY() == _location.getY() + 2 &&( newPlace.getX() == _location.getX() + 1 || newPlace.getX() == _location.getX() - 1))
	{
		_location = newPlace;
		return GOOD_MOVE;
	}
	else if(newPlace.getY() == _location.getY() + 1 &&( newPlace.getX() == _location.getX() + 2|| newPlace.getX() == _location.getX() - 2))
	{
		_location = newPlace;
		return GOOD_MOVE;
	}
	else if (newPlace.getY() == _location.getY() -2 &&( newPlace.getX() == _location.getX() + 1 || newPlace.getX() == _location.getX() - 1))
	{
		_location = newPlace;
		return GOOD_MOVE;
	}
	else if (newPlace.getY() == _location.getY() - 1 &&( newPlace.getX() == _location.getX() + 2 || newPlace.getX() == _location.getX() - 2))
	{
		_location = newPlace;
		return GOOD_MOVE;
	}
	throw int(ERROR_MOVE);

}


Knight::~Knight()
{
}
