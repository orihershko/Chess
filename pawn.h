#pragma once
#include "chessMen.h"
class Pawn : public chessMen
{
private:
	bool _firstStep;
public:
	Pawn(const Place& placeTool, const char type, const bool firstStep);
	int checkMove(const Place& newPlace , chessMen* board[8][8]);
	~Pawn();
};

