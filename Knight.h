#pragma once
#include "chessMen.h"
class Knight :public chessMen
{
public:
	Knight(const Place& location, const char type);
	int checkMove(const Place& newPlace , chessMen* board[8][8]);
	~Knight();
};

