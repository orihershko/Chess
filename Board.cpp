﻿#include "Board.h"
#include<string>
#include"chessMen.h"
#include<iostream>
#define EMPTY '#'
#define Y 8
#define X 8
#define BLACK 0
#define WHITE 1
#define GOOD_MOVE 0
#define CHESS 2
#define ERROR_EXIST_NEW_LOCATION 3
#define ERROR_AFTER_THE_MOVE_CHESS 4
#define ERROR_OVERFLOW 5
#define ERROR_MOVE 6
#define ERROR_SAME_LOCATION 7
using namespace std;

Board::Board(string& data)
{	
	_turn = BLACK;

	//sending the string 
	for (int i = 7; i >= 0; i--)
	{
		for (int b = 0; b < X; b++)
		{
			int static indexString = 0;
			Place piece(b, i);
			if (data[indexString] != EMPTY)
			{
				addToBoard(piece, data[indexString],false);
			}
			else
			{
				_board[i][b] = NULL;
			}
			
			indexString++;
		}
	}

}
Board::~Board()
{
}

void Board::printBoard()
{
	cout << endl;
	for (int i = 7; i >= 0; i--)
	{
		for (int b = 0; b < X; b++)
		{
			if(_board[i][b] != NULL)
				cout << _board[i][b]->getType() << " ";
			else
			{
				cout << EMPTY << " ";
			}
			
		}
		cout << endl;
	}
}
int Board::checkMove(const Place &newLocation, const Place &location)
{

	if (newLocation == location) //error 7
	{
		throw int(ERROR_SAME_LOCATION);
	}
	if (newLocation.getX() > X || newLocation.getY() > Y) // error 5
	{
		throw int(ERROR_OVERFLOW);
	}
	else if (_board[location.getY()][location.getX()] == NULL)//error 2
	{
		throw int(CHESS);
	}
	else if (_turn == WHITE && islower(_board[location.getY()][location.getX()]->getType()) || _turn == BLACK && isupper((_board[location.getY()][location.getX()])->getType())) // error 6
	{
		throw int(ERROR_MOVE); // the aganist player need to play
	}
	else if (_board[newLocation.getY()][newLocation.getX()] != NULL)
	{
		if (_turn == BLACK && islower(_board[newLocation.getY()][newLocation.getX()]->getType()) || _turn == WHITE && isupper((_board[newLocation.getY()][newLocation.getX()])->getType())) // error 3
		{
			throw int(ERROR_EXIST_NEW_LOCATION);
		}
		
		
	}
	int check = _board[location.getY()][location.getX()]->checkMove(newLocation, _board);
	if (!check)
	{
		return check;
	}
	/*
	int c = 0;
	int x = _whiteKing->getLocation().getX();
	for (int i = 0; i < 8; i++)
	{
		if (x == _board[1][i]->getLocation().getX())
		{
			for (int j = 0; j < 8; j++)
			{
				if (_turn == 0 && _board[j][i]->getType == 'R' || _turn == 1 && _board[j][i]->getType == 'r')
				{
					c++;
				}
			}
		}
	}
	*/
	return check;
	
	
	

		
	
	
}

void Board::addToBoard(const Place & NewLocation, char type, bool firstStep)
{
	int x = NewLocation.getX();
	int y = NewLocation.getY();
	if (type == 'r' || type == 'R') //rook
	{
		_board[y][x] = new Rook(NewLocation, type);
	}
	else if (type == 'n' || type == 'N') //knight
	{
		_board[y][x] = new Knight(NewLocation, type);
	}
	else if (type == 'b' || type == 'B') //bishop
	{
		_board[y][x] = new Bishop(NewLocation, type);
	}
	else if (type == 'p' || type == 'P') //pown
	{
		_board[y][x] = new Pawn(NewLocation, type, firstStep);
	}
	else if (type == 'k' || type == 'K') //king
	{
		if (type == 'k')
		{
			_whiteKing = new King(NewLocation, type);
		}
		else if (type == 'K')
		{
			_blackKing = new King(NewLocation, type);
		}
		_board[y][x] = new King(NewLocation, type);
	}
	else if (type == 'q' || type == 'Q') //queen
	{
		_board[y][x] = new Queen(NewLocation, type);
	}
}


void Board::update(const Place& newLocation, const Place& oldlocation)
{
	
	addToBoard(newLocation, _board[oldlocation.getY()][oldlocation.getX()]->getType(),true);
	delete _board[oldlocation.getY()][oldlocation.getX()];
	_board[oldlocation.getY()][oldlocation.getX()] = NULL;
	_turn = !_turn;
}
int Board::getTurn()
{
	return _turn;
}
