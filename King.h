#pragma once
#include "chessMen.h"
class King :public chessMen
{
public:
	King(const Place& location, const char type);
	int checkMove(const Place& newLocation , chessMen* board[8][8]);
	~King();
};

