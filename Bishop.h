#pragma once
#include "chessMen.h"
class Bishop :public chessMen
{
public:
	Bishop(const Place& location, const char type);
	int checkMove(const Place& newPlace , chessMen* board[8][8]);
	~Bishop();
};

