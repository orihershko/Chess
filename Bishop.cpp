#include "Bishop.h"
#include<iostream>


Bishop::Bishop(const Place& location, const char type): chessMen(location, type)
{
}

int Bishop::checkMove(const Place & newPlace , chessMen* board[8][8])
{	
	if (board[newPlace.getY()][newPlace.getX()] != NULL)
	{
		if (isupper(_type) && isupper(board[newPlace.getY()][newPlace.getX()]->getType()) || islower(_type) && islower(board[newPlace.getY()][newPlace.getX()]->getType()))
		{
			throw int(ERROR_MOVE);
		}
	}
	

	int xOp1 = _location.getX() - newPlace.getX();
	int xOp2 = newPlace.getX() - _location.getX();
	int yOp1 = _location.getY() - newPlace.getY();
	int yOp2 = newPlace.getY() - _location.getY();
	//x) location is bigger then newPlace
	//y) location is bigger then newPlace
	if (xOp1 > 0 && yOp1 > 0 && xOp1 == yOp1)
	{
		for (int y = _location.getY()-1; y > newPlace.getY(); y--)
		{
			for (int x = _location.getX()-1; x > newPlace.getX(); x--)
			{
				if (board[y][x] != NULL)
				{
					throw int(ERROR_MOVE);
				}
			}
		}
		_location = newPlace;
		return GOOD_MOVE;
	}
	// x) location is smaller then newPlace
	//y)location is bigger then newPlace
	else if (xOp2 > 0 && yOp1 >0 && xOp2 == yOp1)
	{
		for (int y = _location.getY() - 1; y > newPlace.getY(); y--)
		{
			for (int x = _location.getX() + 1; x < newPlace.getX(); x++)
			{
				if (board[y][x] != NULL)
				{
					throw int(ERROR_MOVE);
				}

			}
		}
		_location = newPlace;
		throw int(ERROR_MOVE);
	}
	//x)location is bigger then newPlace
	//y) location is smaller then newPlace
	else if (xOp1 > 0 && yOp2 >0 && xOp1 == yOp2)
	{
		for (int y = _location.getY()+1; y < newPlace.getY(); y++)
		{
			for (int x = _location.getX()-1; x > newPlace.getX(); x--)
			{
				if (board[y][x] != NULL)
				{
					throw int(ERROR_MOVE);
				}

			}
		}
		_location = newPlace;
		return GOOD_MOVE;
	}
	//x)location is smaller then newPlace
	//y)locayion is smaller then newPlace
	else if (xOp2 > 0 && yOp2 >0 && xOp2 == yOp2)
	{
		for (int y = _location.getY()+1; y < newPlace.getY(); y++)
		{
			for (int x = _location.getX()+1; x < newPlace.getX(); x++)
			{
				if (board[y][x] != NULL)
				{
					throw int(ERROR_MOVE);
				}

			}
		}
		_location = newPlace;
		return GOOD_MOVE;
	}
	throw int(ERROR_MOVE);

	
}


Bishop::~Bishop()
{
}
