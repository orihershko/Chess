#pragma once
#include "chessMen.h"

class Rook : public chessMen
{
public:
	Rook(const Place& startLocation,const char& type);
	~Rook();
	int checkMove(const Place& newLocation , chessMen* board[8][8]);
};

