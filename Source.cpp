#include "Pipe.h"
#include <iostream>
#include <thread>
#include"Board.h"

using namespace std;
void main()
{
	system("start chessGraphics.exe");
	srand(time_t(NULL));
	

	Pipe p;
	bool isConnect = p.connect();
	system("close chessGraphics.exe");
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();

		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE
	string a("r####q##k##############n#######K###########N#########P###Q######1");
	strcpy_s(msgToGraphics, "r####q##k##############n#######K###########N#########P###Q######1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	
	Board board(a);
	board.printBoard();
											  // get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		int x1 = msgFromGraphics[0] - 97;
		int y1 = msgFromGraphics[1] - 49;
		Place location(x1,y1);
		int x2 = msgFromGraphics[2] - 97;
		int y2 = msgFromGraphics[3] - 49;
		Place newLocation(x2,y2);
		try
		{
			int check = board.checkMove(newLocation, location);
			char str[] = { check + 49,NULL };
			strcpy_s(msgToGraphics, str);// msgToGraphics should contain the result of the operation
			board.update(newLocation, location);

		}
		catch (const int e)
		{
			char str[] = { e + 49,NULL };
			strcpy_s(msgToGraphics, str);// msgToGraphics should contain the result of the operation

		}

		board.printBoard();
		// YOUR CODE
		
	


											  // return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}