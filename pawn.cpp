#include "pawn.h"
#include<string>
#define LEFT -1
#define RIGHT 1
#define UP 1
#define DOWN -1
#define UP_FIRST 2
#define DOWN_FIRST -2


Pawn::Pawn(const Place& placeTool,const char type, const bool firsStep): chessMen(placeTool,type)
{
	_firstStep = firsStep;
}

int Pawn::checkMove(const Place& newPlace , chessMen* board[8][8])
{
	
	if ((isupper(_type) && newPlace.getY() == _location.getY() + UP )|| (islower(_type) && newPlace.getY() == _location.getY() + DOWN) )
	{
		if ((newPlace.getX() == _location.getX() || newPlace.getX() == _location.getX()) && board[newPlace.getY()][newPlace.getX()] == NULL)
		{
			_location = newPlace;
			_firstStep = true;
			return GOOD_MOVE;
		}
	}
	if (newPlace.getX() == _location.getX() + RIGHT || newPlace.getX() == _location.getX() + LEFT )
	{
		if (board[newPlace.getY()][newPlace.getX()] == NULL)
		{
			throw int(ERROR_MOVE);
		}
		if ((isupper(_type) && islower(board[newPlace.getY()][newPlace.getX()]->getType()) || islower(_type) && isupper(board[newPlace.getY()][newPlace.getX()]->getType())))
		{
			_location = newPlace;
			_firstStep = true;
			return GOOD_MOVE;
		}
	}
	else if(!_firstStep && (isupper(_type) && newPlace.getY() == _location.getY() + UP_FIRST || islower(_type) && newPlace.getY() == _location.getY() + DOWN_FIRST))
	{
		if ((newPlace.getX() == _location.getX() || newPlace.getX() == _location.getX()) && board[newPlace.getY()][newPlace.getX()] == NULL)
		{
			_location = newPlace;
			_firstStep = true;
			return GOOD_MOVE;
		}
	
	}
	
	
	throw int(ERROR_MOVE);
	
}

Pawn::~Pawn()
{
}
