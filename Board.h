#pragma once
#include "chessMen.h"
#include<string>
#include"Knight.h"
#include"Queen.h"
#include"Rook.h"
#include "Bishop.h"
#include"pawn.h"
#include"King.h"
#define X 8
#define Y 8
class Board
{
public:
	Board(std::string& data);
	~Board();
	void printBoard();
	void update(const Place& newLocation,const Place& oldlocation);
	int checkMove(const Place& NewLocation,const Place& location);
	
	int getTurn();
private:
	void addToBoard(const Place& NewLocation, const char type, bool firstStep);
	chessMen* _board[Y][X]; 
	int _turn; // 1 and lower case is black...0  and upper case is white
	King* _whiteKing;
	King* _blackKing;
};

	
